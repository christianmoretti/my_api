# README


Setup:
```
#!bash
rvm install ruby-2.3.0
rvm use ruby-2.3.0@my_api --create

gem install bunder --no-ri --no-rdoc
bundle install

```


* List:
```
#!bash
curl http://localhost:3000/todos
```

* Create:
```
#!bash
curl -H "Content-Type:application/json; charset=utf-8" -d '{"title":"something to do","order":1,"completed":false}' http://localhost:3000/todos
```